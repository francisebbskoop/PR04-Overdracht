# README.md

In deze folder staan voorbeeldinstanties behorend bij oplevering 0.97 van de
STOP standaard.

De volgende voorbeeldbestanden worden expliciet genoemd in de paklijst van de
oplevering:

## Waterschapsverordening
Deze map bevat paklijst item 36

## Omgevingsplan
Deze map bevat paklijst item 37

## Omgevingsverordening
Deze map bevat paklijst item 38

## AMvB/MR
Deze map bevat paklijst item 39

## Kenmerken
Documentfragmenten met voorbeelden van het annoteren van kenmerken

## Datacollecties
Documentfragmenten met voorbeelden van Datacollecties en Informatieobjecten

## lvbb-berichten
Voorbeelden van berichten naar het LVBB Bronhouderskoppelvlak

## Werkingsgebieden
Documentfragmenten met voorbeelden van het annoteren van werkingsgebieden

## Wijzingingsfragmenten
Documentfragmenten met voorbeelden van (wijzigings)besluiten

