# Overdracht van PR04 deliverables

Dit repository opleveringen van machine leesbare documenten van PR04. Er zijn drie
folders:

 - *xsd*: XML schemas: imop.xsd en stop.xsd. GML schemas worden als service meegeleverd. 
 - *toepassingsprofiel* toepassingsprofielen en modules
 - *voorbeeldbestanden* voorbeeldbestanden

Als deze documenten in deze repository naar elkaar verwijzen gaat dat via
relatieve paden.
